'use strict';

export const availableModels = [
    {
        name: "ogro",
        folder: "./assets/models/player/",
        extension: "glb"
    },
    {
        name: "torre",
        folder: "./assets/models/tower/",
        extension: "glb"
    }
];
