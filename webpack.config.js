const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = (env, argv) => {
    return {
        mode: env.production ? 'production' : 'development',
        devtool: 'source-map',
        entry: {
            app: './src/main.js'
        },
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: '[name].js'
        },
        resolve: {
            extensions: ['.js']
        },
        devServer: {
            static: {
                directory: path.join(__dirname, 'dist'),
            },
            compress: true,
            port: 9000,
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './index.html',
                favicon: './favicon.png',
            }),
            new CopyPlugin({
                patterns: [
                    {from: 'assets', to: 'assets'}
                ],
            })
        ],
        module: {
            rules: [{
                include: [
                    path.resolve(__dirname, "app")
                ],
                test: /\.js$/,
                exclude: /node_modules/
            }]
        }
    }
}
