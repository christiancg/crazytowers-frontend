'use strict';

import {GeometryUtils} from "./geometryutils";

export class ParticleEmitter extends BABYLON.CustomParticleEmitter {
    constructor(mapSideSize, player) {
        super();
        this.mapSideSize = mapSideSize;
        this.player = player;
        this.particlePositionGenerator = (index, particle, out) => {
            let isOk = false;
            out.y = Math.random() * 2;
            let count = 0;
            const maximumVisibleRange = this._getPlayer().maximumVisibleRange;
            const position = {
                x: 0,
                z: 0
            };
            while (!isOk && count<10) {
                out.x = position.x = (Math.random() > .5 ? 1 : -1) * Math.random() * maximumVisibleRange * 2;
                out.z = position.z = (Math.random() > .5 ? 1 : -1) * Math.random() * maximumVisibleRange * 2;
                if(this._isInCorrectPosition(this._getPlayer().tower, position))
                    isOk = !GeometryUtils.isInsideVisibilityRange(0, 0, maximumVisibleRange, position);
                count++;
            }
            if(!isOk) {
                out.x = null;
                out.z = null;
                out.y = null;
            }
        };
        this.particleDestinationGenerator = (index, particle, out) => {
            out.x = particle.position.x + (Math.random() > .5 ? 1 : -1) * Math.random() * 3;
            out.y = particle.position.y + (Math.random() > .5 ? 1 : -1) * Math.random() * 3;
            out.z = particle.position.z + (Math.random() > .5 ? 1 : -1) * Math.random() * 3;
        }
    }

    _isInCorrectPosition(tower, position) {
        return tower.posX + position.x < this.mapSideSize && tower.posX + position.x > 0 &&
            tower.posZ + position.z < this.mapSideSize && tower.posZ + position.z > 0;
    }

    /*
    /!**
     * Called by the particle System when the position is computed for the created particle.
     * @param worldMatrix is the world matrix of the particle system
     * @param positionToUpdate is the position vector to update with the result
     * @param particle is the particle we are computed the position for
     * @param isLocal defines if the position should be set in local space
     *!/
    startPositionFunction(worldMatrix, positionToUpdate, particle, isLocal) {
        positionToUpdate.y = Scalar.RandomRange(this.minEmitBox.y, this.maxEmitBox.y);
        let isOk = false;
        let count = 0;
        const maximumVisibleRange = this._getPlayer().maximumVisibleRange;
        let randX = 0;
        let randZ = 0;
        const position = {
            x: randX,
            z: randZ
        };
        const towerX = this._getPlayer().tower.posX;
        const towerZ = this._getPlayer().tower.posZ;
        while (!isOk && count<5) {
            positionToUpdate.x = position.x = randX = Scalar.RandomRange(this.minEmitBox.x, this.maxEmitBox.x) + this._getPlayer().tower.posX;
            positionToUpdate.z = position.z = randZ = Scalar.RandomRange(this.minEmitBox.z, this.maxEmitBox.z) + this._getPlayer().tower.posZ;
            isOk = !GeometryUtils.isInsideVisibilityRange(towerX, towerZ, maximumVisibleRange, position);
            count++;
        }
        if(!isOk) {
            positionToUpdate.x = null;
            positionToUpdate.z = null;
            positionToUpdate.y = null;
        }
        //Vector3.TransformCoordinatesFromFloatsToRef(randX, randY, randZ, worldMatrix, positionToUpdate);
    }
    */

    _getPlayer() {
        return this.player;
    }

    updatePlayer(player){
        this.player = player;
    }
}
