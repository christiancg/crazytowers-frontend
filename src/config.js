'use strict';

const webUrl = "wss://crazytowers.io:9090";
const localUrl = "ws://localhost:8080/web-socket";

export class Config {
    static getUrl() {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        return urlParams.has('local') ? localUrl : webUrl;
    }

    static getMobile(){
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    }
}


