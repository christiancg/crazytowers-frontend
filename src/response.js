'use strict';

class Response {
    constructor(type) {
        this.type = type;
    }
}

export class ConnectResponse extends Response {
    constructor(mapSideSize, player) {
        super("connect");
        this.mapSideSize = mapSideSize;
        this.player = player;
    }
}

export class DeltaResponse extends Response {
    constructor(player, nearbyGatherers, nearbyTowers) {
        super("delta");
        this.player = player;
        this.nearbyGatherers = nearbyGatherers;
        this.nearbyTowers = nearbyTowers;
    }
}

export class CloseResponse extends Response {
    constructor() {
        super("close");
    }
}
