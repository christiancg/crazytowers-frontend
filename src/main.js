'use strict';

import { WebSocketClient } from './websocket';
import { Config } from './config';


window.addEventListener('DOMContentLoaded', () => {
    // get the button
    let btnConnect = document.getElementById('btnConnect');
    btnConnect.onclick = () => {
        let txtName = document.getElementById('txtName');
        if(btnConnect.textContent === "Close") {
            if(window.webSocketClient !== undefined)
                window.webSocketClient.close();
            window.webSocketClient = null;
            btnConnect.textContent = "Connect";
            return;
        }
        let strMessage = null;
        const message = document.getElementById("message");
        if (!txtName.value) {
            strMessage = "Please insert the name";
        } else if(!txtName.value.trim()){
            strMessage = "The name should not consist of only blank spaces";
        } else if(txtName.value.length < 3 || txtName.value.length > 20){
            strMessage = "The name should be between 3 and 20 characters";
        } else {
            btnConnect.textContent = "Close";
            window.webSocketClient = new WebSocketClient(Config.getUrl(), txtName.value);
            const modal = document.getElementById('modal');
            modal.style.display = "none";
            window.webSocketClient.setUp();
        }
        if(message){
            message.innerText = strMessage;
            message.style.display = "block";
        }
    }
});
