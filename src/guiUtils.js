'use strict';

import * as GUI from 'babylonjs-gui';
import {CloseAttackAction, DefenseAction, RangeAttackAction, ToggleMoveTowerAction} from "./action.js";

export class GuiUtils {

    constructor(player, gathererPenalizationInMs, webSocket, isMobile) {
        this.gathererPenalizationInMs = gathererPenalizationInMs;
        this._gui = GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
        this.ranking = this._getRankingTextBox(this._gui);
        this.centeredCounter = this._getCenteredTextBox(this._gui);
        this.centralTimerInterval = null;
        this.webSocket = webSocket;
        this.status = this._getStatusTextBox(player.tower.bricks, this._getHearts(player));
        const rect = this._getRectTextHolder();
        const btnAbandon = this._getQuitButton();
        this._setComposedUpperLeftGui(this._gui, rect, this.status, btnAbandon);
        if (isMobile) {
            this._setUpMobileControls(this._gui);
        }
    }

    get gui() {
        return this._gui;
    }

    get joystick(){
        return this.joystickRect;
    }

    setSliderCallback(cameraRotateCallback) {
        this.slider.onValueChangedObservable.add(cameraRotateCallback);
    }

    _setUpMobileControls(gui) {
        this._setUpActionButtons(gui);
        this._setUpMoveController(gui);
    }

    _setUpMoveController(gui) {
        const rect = new GUI.Rectangle();
        rect.paddingLeft = 50;
        rect.paddingBottom = 50;
        const hRelation = 0.30;
        rect.height = hRelation;
        rect.width = hRelation * (window.innerHeight / window.innerWidth);
        rect.cornerRadius = 100;
        rect.color = "Orange";
        rect.thickness = 4;
        rect.background = "transparent";
        rect.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_BOTTOM;
        rect.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
        this.joystickRect = new GUI.Rectangle();
        const hIRelation = 0.7;
        this.joystickRect.width = hIRelation;
        this.joystickRect.height = hIRelation;
        this.joystickRect.cornerRadius = 100;
        this.joystickRect.color = "Red";
        this.joystickRect.thickness = 4;
        this.joystickRect.background = "Red";
        this.joystickRect.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_CENTER;
        this.joystickRect.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
        rect.addControl(this.joystickRect);
        gui.addControl(rect);
    }

    _setUpActionButtons(gui){
        const rect = new GUI.Rectangle();
        rect.paddingLeft = "5px";
        rect.paddingTop = "5px";
        rect.width = "300px";
        rect.height = "150px";
        rect.cornerRadius = 20;
        rect.color = "Orange";
        rect.thickness = 4;
        rect.background = "transparent";
        rect.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_BOTTOM;
        rect.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
        rect.addControl(this._getRotationSlider());
        const closeAction = () => this.webSocket.sendMessage(new CloseAttackAction());
        const closeAttack = this._getActionButtons("btnCloseAttack", "attack", GUI.Control.VERTICAL_ALIGNMENT_TOP, GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT, closeAction, 20);
        const rangeAction = () => this.webSocket.sendMessage(new RangeAttackAction());
        const rangeAttack = this._getActionButtons("btnRangeAttack", "throw", GUI.Control.VERTICAL_ALIGNMENT_BOTTOM, GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT, rangeAction);
        const defendAction = () => this.webSocket.sendMessage(new DefenseAction());
        const defend = this._getActionButtons("btnDefend", "defend", GUI.Control.VERTICAL_ALIGNMENT_TOP, GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, defendAction, 20);
        const toggleAction = () => this.webSocket.sendMessage(new ToggleMoveTowerAction());
        const toggle = this._getActionButtons("btnToggleMove", "toggle", GUI.Control.VERTICAL_ALIGNMENT_BOTTOM, GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, toggleAction);
        rect.addControl(closeAttack);
        rect.addControl(rangeAttack);
        rect.addControl(defend);
        rect.addControl(toggle);
        gui.addControl(rect);
    }

    _getRotationSlider() {
        this.slider = new BABYLON.GUI.Slider();
        this.slider.minimum = 0;
        this.slider.maximum = 2 * 3 * Math.PI / 2;
        this.slider.value = 3 * Math.PI / 2;
        this.slider.height = "15px";
        this.slider.width = "280px";
        this.slider.color = "black";
        this.slider.borderColor = "red";
        this.slider.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_TOP;
        return this.slider;
    }

    _getActionButtons(buttonName, buttonText, verticalAlignment, horizontalAlignment, action, top = null) {
        const btn = GUI.Button.CreateSimpleButton(buttonName, buttonText);
        btn.verticalAlignment = verticalAlignment;
        btn.horizontalAlignment = horizontalAlignment;
        btn.paddingLeft = "3px";
        btn.paddingTop = "3px";
        btn.paddingBottom = "3px";
        btn.paddingRight = "3px";
        btn.width = "140px";
        btn.height = "60px";
        btn.color = "white";
        btn.background = "red";
        btn.cornerRadius = 10;
        if(top)
            btn.top = top;
        btn.isPointerBlocker = true;
        btn.onPointerClickObservable.add(action);
        return btn;
    }

    _setComposedUpperLeftGui(gui, rect, statusTectBox, btnAbandon) {
        const panel = new GUI.StackPanel();
        panel.isVertical = true;
        panel.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
        panel.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_TOP;
        rect.addControl(statusTectBox);
        panel.addControl(rect);
        panel.addControl(btnAbandon);
        gui.addControl(panel);
    }

    _getQuitButton() {
        const btnAbandon = GUI.Button.CreateImageButton("btnClose", "Exit", "assets/img/exit-icon.png");
        btnAbandon.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_CENTER;
        btnAbandon.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
        btnAbandon.paddingLeft = "3px";
        btnAbandon.paddingTop = "3px";
        btnAbandon.width = "100px";
        btnAbandon.height = "40px";
        btnAbandon.color = "white";
        btnAbandon.background = "red";
        btnAbandon.cornerRadius = 10;
        btnAbandon.isPointerBlocker = true;
        btnAbandon.onPointerClickObservable.add(() => {
            console.log("Closing socket");
            this.webSocket.close();
        });
        return btnAbandon;
    }

    updateStatus(player) {
        this.status.text = "Tower bricks: " + player.tower.bricks + "\nGatherer life: " + this._getHearts(player) + "\nGatherer bricks: " + player.gatherer.bricks;
    }

    updateRanking({ranking}) {
        this.ranking.text = ranking.map(value => value.position + " - " + value.name + " : " + value.bricks).join("\n");
    }

    showPenalization() {
        this.centeredCounter.color = "red";
        this._gui.addControl(this.centeredCounter);
        this._showCenteredRemainingTime(this.gathererPenalizationInMs);
    }

    showGraceTime(graceTimeInMs) {
        this.centeredCounter.color = "green";
        this._gui.addControl(this.centeredCounter);
        this._showCenteredRemainingTime(graceTimeInMs);
    }

    _showCenteredRemainingTime(remainingTime) {
        if (this.centralTimerInterval)
            clearInterval(this.centralTimerInterval);
        this.centralTimerInterval = setInterval(() => {
            if (remainingTime <= 0) {
                this.centeredCounter.text = "";
                clearInterval(this.centralTimerInterval);
                this._gui.removeControl(this.centeredCounter);
                this.centralTimerInterval = null;
            } else {
                const secondToShow = remainingTime / 1000;
                this.centeredCounter.text = secondToShow.toString();
                remainingTime -= 100;
            }
        }, 100);
    }

    _getCenteredTextBox(gui) {
        const graceGui = new GUI.TextBlock();
        graceGui.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
        graceGui.textVerticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_CENTER;
        graceGui.fontSize = 250;
        graceGui.color = "green";
        gui.addControl(graceGui);
        return graceGui;
    }

    _getRectTextHolder() {
        const rect = new GUI.Rectangle();
        rect.paddingLeft = "5px";
        rect.paddingTop = "5px";
        rect.width = "300px";
        rect.height = "100px";
        rect.cornerRadius = 20;
        rect.color = "Orange";
        rect.thickness = 4;
        rect.background = "green";
        rect.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_TOP;
        rect.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
        return rect;
    }

    _getStatusTextBox(bricks, hearts) {
        const statusTextBox = new GUI.TextBlock();
        statusTextBox.paddingLeft = "5px";
        statusTextBox.paddingTop = "5px";
        statusTextBox.text = "Tower bricks: " + bricks + "\nGatherer life: " + hearts + "\nGatherer bricks: 0";
        statusTextBox.background = "green";
        statusTextBox.color = "white";
        statusTextBox.fontSize = 24;
        statusTextBox.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
        statusTextBox.textVerticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_TOP;
        return statusTextBox;
    }

    _getRankingTextBox(gui) {
        const rankingTextBox = new GUI.TextBlock();
        rankingTextBox.color = "white";
        rankingTextBox.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
        rankingTextBox.textVerticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_TOP;
        rankingTextBox.fontSize = 24;
        gui.addControl(rankingTextBox);
        return rankingTextBox;
    }

    _getHearts(player) {
        let hearts = "";
        for (let i = 0; i < player.gatherer.health; i++)
            hearts = hearts.concat(String.fromCodePoint(0x2764));
        return hearts;
    }
}
