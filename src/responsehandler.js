'use strict';

import { SceneController } from './scene';

export class ResponseHandler {
    constructor(webSocket, isMobile) {
        this.sController = new SceneController(webSocket, isMobile);
    }

    handle(response) {
        switch (response.type) {
            case "connect":
                this._handleConnectResponse(response);
                break;
            case "delta":
                this._handleDeltaResponse(response);
                break;
            case "dead":
                this._handleDeadResponse();
                break;
            case "ranking":
                this._handleRankingResponse(response);
                break;
            case "move":
            case "defend":
            case "closeAttack":
            case "rangeAttack":
            case "toggleMoveTower":
            case "stopMove":
                if(!response.status)
                    this._handleIncorrectActionResponse(response);
                break;
            case "close":
                this._handleClose();
                break;
            default:
                console.log("Unsupported response type: " + response.type);
        }
    }

    _handleDeltaResponse(response) {
        this.sController.updateScene(response);
    }

    _handleRankingResponse(response) {
        this.sController.updateRanking(response);
    }

    _handleIncorrectActionResponse(response) {
        this.sController.cancelAction(response);
    }

    _handleDeadResponse() {
        const message = document.getElementById("message");
        message.innerText = "You are dead!";
        message.style.display = "block";
        this.sController.disposeAll();
    }

    _handleConnectResponse(response) {
        this.sController.createAndRender(response);
    }

    _handleClose() {
        this.sController.disposeAll();
    }
}
