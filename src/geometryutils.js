'use strict';

export class GeometryUtils {
    static getCameraHeight(newBricks, oldBricks, mapSideSize, startupCameraHeight, camera) {
        if (newBricks > oldBricks && newBricks < mapSideSize) {
            return startupCameraHeight + (newBricks / 5);
        } else if (newBricks < oldBricks && newBricks > startupCameraHeight && newBricks < mapSideSize) {
            return camera.heightOffset - (camera.heightOffset - newBricks);
        } else
            return camera.radius;
    }

    static calculateSpotlightAngle(radius, height) {
        return Math.atan(radius / height) * 2;
    }

    static calculateHeading(startY, startX, moveY, moveX) {
        return Math.atan2(moveY - startY, moveX - startX);
    }

    static randomPosInMapAvoidingFOV(mapSideSize, towerPosX, towerPosZ, bricks, startingBricks) {
        const maximumRangeForBricks = Math.max(bricks / 2.0, startingBricks * 2.0);
        const position = {
            x: 0,
            z: 0
        };
        let isCorrectPosition = false;
        while (!isCorrectPosition) {
            position.x = this._randomBetweenRange(0, mapSideSize);
            position.z = this._randomBetweenRange(0, mapSideSize);
            if(!this.isInsideVisibilityRange(towerPosX,towerPosZ, maximumRangeForBricks, position))
                isCorrectPosition = true;
        }
        return position;
    }

    static isInsideVisibilityRange(posX, posY, height, toCheck) {
        return Math.hypot(Math.abs(toCheck.z - posY), Math.abs(toCheck.x - posX)) < height;
    }

    static _randomBetweenRange(min, max) {
        return Math.random() * (max - min) + min;
    }
}
