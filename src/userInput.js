'use strict';

import {Pointer} from "./models";
import {
    CloseAttackAction,
    DefenseAction,
    MoveAction,
    RangeAttackAction,
    StopMoveAction,
    ToggleMoveTowerAction
} from "./action";
import {GeometryUtils} from "./geometryutils";

export class UserInput {
    constructor(isMobile, webSocket, scene, camera, joystickRect, tower, gatherer) {
        this.isMobile = isMobile;
        this.webSocket = webSocket;
        this.scene = scene;
        this.camera = camera;
        if (this.isMobile) {
            this.mobileMovementControlDiv = document.getElementById('mobileMovementControlDiv');
            this.mobileMovementControlDiv.hidden = false;
            this.joysticRect = joystickRect;
            this.originalCenterX = joystickRect.centerX;
            this.originalCenterY = joystickRect.centerY;
            this.tower = tower;
            this.gatherer = gatherer;
            this.rotationOffset = 0;
        }
        this.mouseClickEventProcessor = () => {
            let pickedPosX = null;
            let pickedPosZ = null;
            const pickResult = this.scene.pick(this.scene.pointerX, this.scene.pointerY);
            if (pickResult.hit) {
                pickedPosX = pickResult.pickedPoint.x;
                pickedPosZ = pickResult.pickedPoint.z;
                const pointer = new Pointer(pickedPosX, pickedPosZ);
                if (!this.drawablePointer)
                    this.drawablePointer = Pointer.getDrawable(pointer, this.scene);
                else {
                    this.drawablePointer.dataObject.posX = pointer.posX;
                    this.drawablePointer.dataObject.posZ = pointer.posZ;
                    this.drawablePointer.drawableObject.position.x = pointer.posX;
                    this.drawablePointer.drawableObject.position.z = pointer.posZ;
                }
                this.webSocket.sendMessage(new MoveAction(null, pointer.posX, pointer.posZ));
            }
        };
        this.keyboardPressEventProcessor = (event) => {
            switch (event.key) {
                case 'a':
                case 'A':
                    this.webSocket.sendMessage(new CloseAttackAction());
                    break;
                case ' ':
                    this.webSocket.sendMessage(new RangeAttackAction());
                    break;
                case 'd':
                case 'D':
                    this.webSocket.sendMessage(new DefenseAction());
                    break;
                case 't':
                case 'T':
                    this.webSocket.sendMessage(new ToggleMoveTowerAction());
                    break;
                case 'ArrowLeft':
                    this.camera.rotationOffset += 10;
                    break;
                case 'ArrowRight':
                    this.camera.rotationOffset -= 10;
                    break;
                default:
                    break;
            }
        };
        this.mouseWheelEventProcessor = (event) => {
            if (event.deltaY !== 0) {
                this.camera.rotationOffset += event.deltaY / 10;
            }
        };
        this.startTouchEventProcessor = (event) => {
            event.preventDefault();
            const touch = event.touches[0];
            this.touchX = touch.clientX;
            this.touchY = touch.clientY;
        };
        this.moveTouchEventProcessor = (event) => {
            event.preventDefault();
            const touch = event.touches[0];
            const movedX = touch.clientX;
            const movedY = touch.clientY;
            let heading = GeometryUtils.calculateHeading(this.touchY, this.touchX, movedY, movedX);
            this.joysticRect.left = this.originalCenterX + 10 * Math.cos(heading);
            this.joysticRect.top = this.originalCenterY + 10 * Math.sin(heading);
            heading = GeometryUtils.calculateHeading(movedY, this.touchX, this.touchY, movedX) + this.rotationOffset;
            this.webSocket.sendMessage(new MoveAction(heading));
        };
        this.endTouchEventProcessor = (event) => {
            event.preventDefault();
            this.joysticRect.left = this.originalCenterX;
            this.joysticRect.top = this.originalCenterY;
            this.webSocket.sendMessage(new StopMoveAction());
        };
    }

    setRotationOffset(rotationOffset) {
        this.rotationOffset = rotationOffset - 3 * Math.PI / 2;
    }

    registerUserInputs() {
        if (!this.isMobile) {
            window.addEventListener("keydown", this.keyboardPressEventProcessor);
            window.addEventListener("pointerdown", this.mouseClickEventProcessor);
            window.addEventListener("wheel", this.mouseWheelEventProcessor);
        } else {
            this.mobileMovementControlDiv.addEventListener("touchstart", this.startTouchEventProcessor);
            this.mobileMovementControlDiv.addEventListener("touchmove", this.moveTouchEventProcessor);
            this.mobileMovementControlDiv.addEventListener("touchend", this.endTouchEventProcessor);
        }
    }

    unregisterUserInputs() {
        if (!this.isMobile) {
            window.removeEventListener("keydown", this.keyboardPressEventProcessor);
            window.removeEventListener("pointerdown", this.mouseClickEventProcessor);
            window.removeEventListener("wheel", this.mouseWheelEventProcessor);
        } else {
            this.mobileMovementControlDiv.removeEventListener("touchstart", this.startTouchEventProcessor);
            this.mobileMovementControlDiv.removeEventListener("touchmove", this.moveTouchEventProcessor);
            this.mobileMovementControlDiv.removeEventListener("touchend", this.endTouchEventProcessor);
        }
    }
}