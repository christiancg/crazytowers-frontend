'use strict';

import * as BABYLON from 'babylonjs';
import * as GUI from 'babylonjs-gui';

function getLabel(name, offset, mesh, ui) {
    const textBlock = new GUI.TextBlock();
    textBlock.text = name;
    textBlock.color = "black";
    textBlock.fontSize = 12;
    textBlock.width = "150px";
    textBlock.height = "20px";
    ui.addControl(textBlock);
    textBlock.linkWithMesh(mesh);
    textBlock.linkOffsetY = offset;
    return textBlock;
}

class Locatable {
    constructor(id, name, posX, posZ, heading) {
        this.id = id;
        this.name = name;
        this.posX = posX;
        this.posZ = posZ;
        this.heading = heading;
    }
}

class Drawable {
    constructor(dataObject, drawableObject, label) {
        this.dataObject = dataObject;
        this.drawableObject = drawableObject;
        this.label = label;
    }

    dispose(){
        this.drawableObject.dispose();
        if(this.label)
            this.label.dispose();
    }

    toggleVisibility() {
        this.drawableObject.setEnabled(!this.drawableObject.isEnabled());
        this.label.isVisible = !this.label.isVisible;
    }

    setVisibility(isVisible) {
        this.drawableObject.setEnabled(isVisible);
        this.label.isVisible = isVisible;
    }

    get visible(){
        return this.drawableObject.isEnabled();
    }
}

export class Player {
    constructor(id, name, realm, tower, gatherer, maximumVisibleRange) {
        this.id = id;
        this.name = name;
        this.realm = realm;
        this.tower = tower;
        this.gatherer = gatherer;
        this.maximumVisibleRange = maximumVisibleRange;
    }
}

export class Tower extends Locatable {
    static getDrawable({id, name, posX, posZ, bricks, heading}, scene, ui, assetLoader) {
        let aux = new Tower(id, name, posX, posZ, heading);
        let auxD = assetLoader.getMesh('torre');
        auxD.position = new BABYLON.Vector3(posX, auxD.position.y, posZ);
        auxD.rotate(BABYLON.Axis.Y, heading, BABYLON.Space.WORLD);
        const label = getLabel(name, 40, auxD, ui);
        return new Drawable(aux, auxD, label);
    }

    constructor(id, name, posX, posZ, heading) {
        super(id, name, posX, posZ, heading);
        this.bricks = 10;
    }
}

export class Gatherer extends Locatable {
    static getDrawable({id, name, posX, posZ, heading}, scene, ui, assetLoader) {
        let aux = new Gatherer(id, name, posX, posZ, heading);
        let auxD = assetLoader.getMesh('ogro');
        auxD.position = new BABYLON.Vector3(posX, auxD.position.y, posZ);
        auxD.rotate(BABYLON.Axis.Y, heading, BABYLON.Space.WORLD);
        const label = getLabel(name, 30, auxD, ui);
        return new Drawable(aux, auxD, label);
    }

    constructor(id, name, posX, posZ, heading) {
        super(id, name, posX, posZ, heading);
        this.health = 100;
        this.speed = 1;
        this.angle = 0;
        this.active = true;
        this.timestampPenalizationTime = -1;
    }
}

export class Brick extends Locatable {
    static getDrawable({id, posX, posZ, large, heading}, scene) {
        let aux = new Brick(id, posX, posZ, large);
        let auxD;
        if (large)
            auxD = BABYLON.MeshBuilder.CreateBox(`b-${id}`, {height: 2, width: 6, depth: 3}, scene);
        else
            auxD = BABYLON.MeshBuilder.CreateBox(`b-${id}`, {height: 1, width: 4, depth: 2}, scene);
        auxD.position = new BABYLON.Vector3(posX, 0.1, posZ);
        auxD.rotate(BABYLON.Axis.Y, heading, BABYLON.Space.WORLD);
        let myMaterial = new BABYLON.StandardMaterial("myMaterial", scene);
        myMaterial.diffuseColor = new BABYLON.Color3(1, 1, 0);
        auxD.material = myMaterial;
        return new Drawable(aux, auxD);
    }

    constructor(id, posX, posZ, large, heading) {
        super(id, "", posX, posZ, heading);
        this.large = large;
    }
}

export class AttackBrick extends Locatable {
    static getDrawable({id, posX, posZ, heading}, scene) {
        let aux = new AttackBrick(id, posX, posZ);
        let auxD = BABYLON.MeshBuilder.CreateBox(`b-${id}`, {height: 2, width: 6, depth: 3}, scene);
        auxD.position = new BABYLON.Vector3(posX, 0.1, posZ);
        auxD.rotate(BABYLON.Axis.Y, heading, BABYLON.Space.WORLD);
        let myMaterial = new BABYLON.StandardMaterial("myMaterial", scene);
        myMaterial.diffuseColor = new BABYLON.Color3(1, 1, 0);
        auxD.material = myMaterial;
        return new Drawable(aux, auxD);
    }

    constructor(id, posX, posZ, heading) {
        super(id, "", posX, posZ, heading);
    }
}

export class Pointer extends Locatable {
    static getDrawable({posX, posZ}, scene) {
        const aux = new Pointer(posX, posZ);
        let auxD = BABYLON.MeshBuilder.CreatePolyhedron("pointer", {type: 10, size: 5, scene});
        auxD.position = new BABYLON.Vector3(posX, 3, posZ);
        let myMaterial = new BABYLON.StandardMaterial("myMaterial", scene);
        myMaterial.diffuseColor = new BABYLON.Color3(0.5, 0.5, 1);
        auxD.material = myMaterial;
        return new Drawable(aux, auxD);
    }

    constructor(posX, posZ) {
        super("pointer", "pointer", posX, posZ);
    }
}
