'use strict';

import * as BABYLON from 'babylonjs';
import 'babylonjs-loaders';
import {availableModels} from "../assets/models/availableModels";

export class AssetLoader {
    constructor(scene, afterAssetsLoadedClosure) {
        this.assetsManager = new BABYLON.AssetsManager(scene);
        this.descriptors = {};
        this.meshes = {};
        this._assetLoaderTask();
        this.assetsManager.onFinish = () => {
            availableModels.forEach(value => {
                this.meshes[value.name].position.y = this.descriptors[value.name].yOffset;
                const scalingFactor = this.descriptors[value.name].scalingFactor;
                this.meshes[value.name].scaling = new BABYLON.Vector3(scalingFactor, scalingFactor, scalingFactor);
            });
            afterAssetsLoadedClosure();
        };
        this.assetsManager.load();
    }

    _assetLoaderTask() {
        availableModels.forEach(value => {
            let meshTask = this.assetsManager.addMeshTask(value.name + " task", "", value.folder, value.name + "." + value.extension);
            meshTask.onSuccess = (task) => {
                task.loadedMeshes[0].setEnabled(false);
                this.meshes[value.name] = task.loadedMeshes[0];
            };
            let descriptorTask = this.assetsManager.addTextFileTask(value.name + " json task", value.folder + value.name + ".json");
            descriptorTask.onSuccess = (task) => {
                this.descriptors[value.name] = JSON.parse(task.text);
            };
        });
    }

    getMesh(meshName) {
        return this.meshes.hasOwnProperty(meshName) ? this.meshes[meshName].clone() : null;
    }
}
