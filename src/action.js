'use strict';

class Action {
    constructor(type) {
        this.type = type;
    }
}

export class ConnectAction extends Action {
    constructor(name, isMobile) {
        super("connect");
        this.name = name;
        this.isMobile = isMobile;
    }
}

export class MoveAction extends Action {
    constructor(heading, posX, posZ) {
        super("move");
        if (heading) {
            this.heading = heading;
        } else {
            this.posX = posX;
            this.posZ = posZ;
        }
    }
}

export class CloseAttackAction extends Action {
    constructor() {
        super("closeAttack");
    }
}

export class RangeAttackAction extends Action {
    constructor() {
        super("rangeAttack");
    }
}

export class DefenseAction extends Action {
    constructor() {
        super("defend");
    }
}

export class ToggleMoveTowerAction extends Action {
    constructor() {
        super("toggleMoveTower");
    }
}

export class StopMoveAction extends Action {
    constructor() {
        super("stopMove");
    }
}
