'use strict';

import {ConnectAction} from './action';
import {ResponseHandler} from './responsehandler';
import {CloseResponse} from './response';
import {Config} from "./config";

export class WebSocketClient {
    constructor(url, name) {
        this.name = name;
        this.isMobile = Config.getMobile();
        this.hasConnected = false;
        this.socket = new WebSocket(url);
        this.handler = new ResponseHandler(this, this.isMobile);
    }

    setUp() {
        let self = this;
        this.socket.addEventListener('open', () => {
            this.hasConnected = true;
            this.sendMessage(new ConnectAction(self.name, this.isMobile));
        });
        this.socket.addEventListener('message', event => {
            let response = JSON.parse(event.data);
            this.handler.handle(response);
        });
        this.socket.onclose = () => {
            if (this.hasConnected)
                this.close();
            else {
                this._showMenu("Error while connecting to server");
                console.log("WebSocket connection error")
            }
            this.hasConnected = false;
        };
        this.socket.onerror = (evt) => {
            if (this.socket.readyState === 1) {
                console.log('WebSocket normal error: ' + evt.type);
            }
        };
    }

    _showMenu(message) {
        const messageItem = document.getElementById("message");
        if (message) {
            messageItem.innerText = message;
            messageItem.style.display = "block";
        } else if (!messageItem.innerText)
            messageItem.style.display = "none";
        let btnConnect = document.getElementById('btnConnect');
        btnConnect.textContent = "Connect";
        const modal = document.getElementById('modal');
        modal.style.display = "block";
    }

    close() {
        this.handler.handle(new CloseResponse());
        this.socket.close();
        this.socket.onclose = null;
        this._showMenu();
        console.log("WebSocket is closed now.");
    }

    sendMessage(action) {
        this.socket.send(JSON.stringify(action));
    }
}
