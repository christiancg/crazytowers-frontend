'use strict';

import * as BABYLON from 'babylonjs';
import {AttackBrick, Brick, Gatherer, Player, Tower} from './models';
import {GeometryUtils} from "./geometryutils";
import {GuiUtils} from "./guiUtils";
import {AssetLoader} from "./assetLoader";
import {UserInput} from "./userInput";
import {ParticleEmitter} from "./customParticleEmitter";

export class SceneController {
    constructor(webSocket, isMobile) {
        this.canvas = document.getElementById('renderCanvas');
        this.engine = new BABYLON.Engine(this.canvas, true);
        this.player = null;
        this.camera = null;
        this.light = null;
        this.scene = null;
        this.particleSystem = null;
        this.customEmitter = null;
        this.fountain = null;
        this.assetLoader = null;
        this.hasFinishedLoading = false;
        this.guiManager = null;
        this.lastPenalization = -1;
        this.startingBricks = 0;
        this.ownDrawableTower = null;
        this.ownDrawableGatherer = null;
        this.drawableTowers = [];
        this.drawableGatherers = [];
        this.drawableBricks = [];
        this.drawableAttackBricks = [];
        this.drawablePointer = null;
        this.webSocket = webSocket;
        this.isMobile = isMobile;
        this.userInput = null;
        this.minGpuParticleCount = 10000;
        this.minCpuParticleCount = 5000;
        this.gpuParticles = false;
    }

    _setParticleSystem() {
        if (BABYLON.GPUParticleSystem.IsSupported && this.gpuParticles) {
            this.particleSystem = new BABYLON.GPUParticleSystem("particles", { capacity: 50000 }, this.scene);
            this.particleSystem.activeParticleCount = this.minGpuParticleCount;
            this.particleSystem.manualEmitCount = this.particleSystem.activeParticleCount;
            this.particleSystem.emitRate = this.minGpuParticleCount;
        } else {
            this.particleSystem = new BABYLON.ParticleSystem("particles", this.minCpuParticleCount, this.scene);
            this.particleSystem.emitRate = this.minCpuParticleCount;
        }
        this.particleSystem.particleTexture = new BABYLON.Texture("../assets/textures/smoke.png", this.scene);
        this.particleSystem.color1 = new BABYLON.Color4(0.8, 0.8, 0.8, 0.2);
        this.particleSystem.color2 = new BABYLON.Color4(.95, .95, .95, 0.4);
        this.particleSystem.colorDead = new BABYLON.Color4(0.9, 0.9, 0.9, 0.3);
        this.particleSystem.minSize = 3.5;
        this.particleSystem.maxSize = 5.0;
        this.particleSystem.minLifeTime = 0.8;
        this.particleSystem.maxLifeTime = 2.5;
        this.particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD;
        this.particleSystem.gravity = new BABYLON.Vector3(0, 0, 0);
        this.particleSystem.minAngularSpeed = -2;
        this.particleSystem.maxAngularSpeed = 2;
        this.particleSystem.minEmitPower = .5;
        this.particleSystem.maxEmitPower = 1;
        this.particleSystem.updateSpeed = 0.005;
        this.customEmitter = new ParticleEmitter(this.mapSideSize, this.player);
        this.particleSystem.particleEmitterType = this.customEmitter;
        this.fountain = BABYLON.Mesh.CreateBox("fountain", 0.1, this.scene);
        this.fountain.visibility = 0;
        this.particleSystem.emitter = this.fountain;
        this.particleSystem.start();
    }

    _updateParticleSystem(player) {
        if(this.gpuParticles)
            this.particleSystem.activeParticleCount = this.minGpuParticleCount * player.maximumVisibleRange / 5;
        else
            this.particleSystem.capacity = this.minCpuParticleCount * player.maximumVisibleRange / 5;
        this.fountain.position.x = player.tower.posX;
        this.fountain.position.z = player.tower.posZ;
        this.customEmitter.updatePlayer(player);
    }

    setupCamera(tower, towerMesh, scene, isMobile) {
        const cameraPosition = new BABYLON.Vector3(tower.posX, this.startupCameraHeight, tower.posZ - 10);
        this.startupCameraHeight = tower.bricks * 3;
        if (!isMobile) {
            this.camera = new BABYLON.FollowCamera('followCamera', cameraPosition, scene);
            this.camera.radius = this.startupCameraHeight;
            this.camera.heightOffset = this.startupCameraHeight;
            this.camera.rotationOffset = 0;
            this.camera.cameraAcceleration = 0.01;
            this.camera.maxCameraSpeed = 10;
        } else {
            this.camera = new BABYLON.ArcRotateCamera('arcRotateCamera',  3 * Math.PI / 2, Math.PI / 3.5, this.startupCameraHeight, towerMesh.position, scene);
        }
        this.camera.lockedTarget = towerMesh;
    }

    disposeAll() {
        this.userInput.unregisterUserInputs();
        if (this.guiManager.gui)
            this.guiManager.gui.dispose();
        //if (this.scene)
        //    this.scene.dispose();
        if (this.engine) {
            this.engine.clear();
            this.engine.dispose();
        }
    }

    _setGround(mapSideSize, selectedGroundTexture) {
        const ground = BABYLON.Mesh.CreateGround('ground1', mapSideSize, mapSideSize, 1, this.scene);
        ground.position.x = mapSideSize / 2;
        ground.position.z = mapSideSize / 2;
        const backgroundMaterial = new BABYLON.StandardMaterial("backgroundMaterial", this.scene);
        backgroundMaterial.diffuseTexture = new BABYLON.Texture("./assets/textures/ground/" + selectedGroundTexture + ".jpg", this.scene);
        backgroundMaterial.diffuseTexture.uScale = 5.0;//Repeat 5 times on the Vertical Axes
        backgroundMaterial.diffuseTexture.vScale = 5.0;//Repeat 5 times on the Horizontal Axes
        backgroundMaterial.shadowLevel = 0.4;
        backgroundMaterial.primaryColor = BABYLON.Color3.White();
        backgroundMaterial.primaryLevel = 1;
        backgroundMaterial.secondaryLevel = 0;
        backgroundMaterial.tertiaryLevel = 0;
        backgroundMaterial.useRGBColor = false;
        ground.material = backgroundMaterial;
        ground.receiveShadows = true;
    }

    _setUpFog(distance) {
        this.scene.fogMode = BABYLON.Scene.FOGMODE_LINEAR;
        this.scene.fogColor = new BABYLON.Color3(0.9, 0.9, 0.85);
        this.scene.fogStart = distance;
        this.scene.fogEnd = distance * 3;
    }

    _updateFog(distance) {
        this.scene.fogStart = distance * 2;
        this.scene.fogEnd = distance * 4;
    }

    afterAssetsLoaded() {
        this.ownDrawableTower = Tower.getDrawable(this.player.tower, this.scene, this.guiManager.gui, this.assetLoader);
        this.ownDrawableGatherer = Gatherer.getDrawable(this.player.gatherer, this.scene, this.guiManager.gui, this.assetLoader);
        this.setLights(this.ownDrawableTower.drawableObject);
        this._addToShadowGenerator(this.ownDrawableTower.drawableObject);
        this._addToShadowGenerator(this.ownDrawableGatherer.drawableObject);
        this.setupCamera(this.player.tower, this.ownDrawableTower.drawableObject, this.scene, this.isMobile);
        this.hasFinishedLoading = true;
        this.engine.runRenderLoop(() => {
            if (this.scene) {
                this.scene.render();
            }
        });
        this.userInput = new UserInput(this.isMobile, this.webSocket, this.scene, this.camera, this.guiManager.joystick, this.ownDrawableTower, this.ownDrawableGatherer);
        this.userInput.registerUserInputs();
        if(this.isMobile){
            const scrollCallBack = (value) => {
                this.camera.alpha = value;
                this.userInput.setRotationOffset(value);
            };
            this.guiManager.setSliderCallback(scrollCallBack);
        }
        this._setUpFog(this.startupCameraHeight);
        this._setParticleSystem();
    }

    createAndRender({mapSideSize, positionRefreshIntervalInMs, gathererPenalizationInMs, graceTimeInMs, player}) {
        this.player = player;
        this.mapSideSize = mapSideSize;
        this.startingBricks = player.tower.bricks;
        this.positionRefreshIntervalInMs = positionRefreshIntervalInMs;
        this.scene = new BABYLON.Scene(this.engine);
        this.guiManager = new GuiUtils(player, gathererPenalizationInMs, this.webSocket, this.isMobile);
        this._setGround(mapSideSize, "wood-chips");
        const afterAssetsLoadedClosure = () => {
            this.afterAssetsLoaded();
        };
        this.assetLoader = new AssetLoader(this.scene, afterAssetsLoadedClosure);
        window.addEventListener('resize', () => {
            this.engine.resize();
        });
        this.guiManager.showGraceTime(graceTimeInMs);
    }

    updateScene({player, nearbyGatherers, nearbyTowers, nearbyBricks, nearbyAttackBricks}) {
        if (this.scene && this.hasFinishedLoading) {
            this.player = new Player(player.id, player.name, player.realm, player.tower, player.gatherer, player.maximumVisibleRange);
            const distance = GeometryUtils.getCameraHeight(player.tower.bricks, this.ownDrawableTower.dataObject.bricks, this.mapSideSize, this.startupCameraHeight, this.camera);
            this._updateOwnGatherer(player.gatherer);
            this._updateCamera(distance);
            this._updateOwnTower(player.tower);
            this._updateGatherers(nearbyGatherers);
            this._updateTowers(nearbyTowers);
            this._updateBricks(nearbyBricks);
            this._updateAttackBricks(nearbyAttackBricks);
            this._updateStatus(player);
            this._updateLight(player);
            this._updateFog(distance);
            this._updateParticleSystem(player);
        }
    }

    updateRanking(rankingResponse) {
        this.guiManager.updateRanking(rankingResponse);
    }

    cancelAction({type}) {
        console.log('Action sent of type "' + type + '" was incorrect');
    }

    setLights(drawableTower) {
        const position = new BABYLON.Vector3(0, 30, 0);
        const direction = new BABYLON.Vector3(0, -10, 0);
        this.light = new BABYLON.SpotLight("spotLight", position, direction, Math.PI / 100, 10, this.scene);
        this.light.parent = drawableTower;
        this.light.diffuse = new BABYLON.Color3(0.8, 0.8, 0.8);
        this.light.specular = new BABYLON.Color3(0.8, 0.8, 0.8);
        const directionalLightDirection = new BABYLON.Vector3(0, -2, -1);
        const directionalLight = new BABYLON.DirectionalLight('directionalLight', directionalLightDirection, this.scene);
        //directionalLight.diffuse = new BABYLON.Color3(0.4, 0.4, 0.4);
        //directionalLight.specular = new BABYLON.Color3(0.8, 0.8, 0.8);
        directionalLight.intensity = 0.3;
        this.shadowGenerator = new BABYLON.ShadowGenerator(1024, directionalLight);
        //this.shadowGenerator.usePoissonSampling = true;
    }

    _updateLight(player) {
        const range = player.maximumVisibleRange / 100;
        const height = this.light.position.y;
        this.light.angle = GeometryUtils.calculateSpotlightAngle(range, height);
    }

    _updateStatus(player) {
        this.guiManager.updateStatus(player);
    }

    _updateCamera(distance) {
        if (!this.isMobile)
            this.camera.heightOffset = distance;
        this.camera.radius = distance;
    }

    _updateOwnGatherer(gatherer) {
        if (gatherer.active !== this.ownDrawableGatherer.dataObject.active) {
            this.ownDrawableGatherer.toggleVisibility();
            this.ownDrawableGatherer.dataObject.active = gatherer.active;
            if (gatherer.active) {
                this.lastPenalization = gatherer.timestampPenalizationTime;
                this.guiManager.showPenalization();
            }
        }
        if (gatherer.timestampPenalizationTime > 0 && this.lastPenalization !== gatherer.timestampPenalizationTime) {
            if (this.ownDrawableGatherer.visible)
                this.ownDrawableGatherer.setVisibility(false);
            this.guiManager.showPenalization();
        } else if (gatherer.active) {
            if (!this.ownDrawableGatherer.visible)
                this.ownDrawableGatherer.setVisibility(true);
            this._updateOwnGeneric(gatherer, 'ownDrawableGatherer');
        }
    }

    _updateOwnTower(tower) {
        this.ownDrawableTower.dataObject.bricks = tower.bricks;
        this._updateOwnGeneric(tower, 'ownDrawableTower');
    }

    _updateOwnGeneric(object, name) {
        this[name].dataObject.posX = object.posX;
        this[name].dataObject.posZ = object.posZ;
        this[name].drawableObject.position.x = object.posX;
        this[name].drawableObject.position.z = object.posZ;
        if (this[name].dataObject.heading !== object.heading)
            this[name].drawableObject.rotate(BABYLON.Axis.Y, this[name].dataObject.heading - object.heading, BABYLON.Space.WORLD);
        this[name].dataObject.heading = object.heading;
    }

    _updateGatherers(nearbyGatherers) {
        this._updateGeneric(nearbyGatherers, Gatherer, 'drawableGatherers');
    }

    _updateTowers(nearbyTowers) {
        // const updateFunction = (oldTower, newTower) => {
        //     const scaling = newTower.bricks / oldTower.dataObject.bricks;
        //     if (scaling !== 1)
        //         oldTower.drawableObject.scaling.y = scaling;
        //     oldTower.dataObject.bricks = newTower.bricks;
        //  };
        // this._updateGeneric(nearbyTowers, Tower, 'drawableTowers', updateFunction);
        this._updateGeneric(nearbyTowers, Tower, 'drawableTowers');
    }

    _updateBricks(nearbyBricks) {
        this._updateGeneric(nearbyBricks, Brick, 'drawableBricks');
    }

    _updateAttackBricks(nearbyAttackBricks) {
        this._updateGeneric(nearbyAttackBricks, AttackBrick, 'drawableAttackBricks');
    }

    _updateGeneric(nearbyItems, type, drawableListName, specificUpdateFunction) {
        let itemsToAdd = [];
        nearbyItems.forEach(auxItem => {
            let foundItem = this[drawableListName]
                .filter(x => auxItem.id === x.dataObject.id);
            if (foundItem.length > 0) {
                foundItem[0].dataObject.posX = auxItem.posX;
                foundItem[0].dataObject.posZ = auxItem.posZ;
                foundItem[0].drawableObject.position.x = auxItem.posX;
                foundItem[0].drawableObject.position.z = auxItem.posZ;
                if (specificUpdateFunction)
                    foundItem[0] = specificUpdateFunction(foundItem[0], auxItem);
            } else {
                const drawable = type.getDrawable(auxItem, this.scene, this.guiManager.gui, this.assetLoader);
                itemsToAdd.push(drawable);
                this._addToShadowGenerator(drawable.drawableObject);
            }
        });
        this[drawableListName] = this[drawableListName].concat(itemsToAdd);
        const toDispose = this[drawableListName].filter(x => nearbyItems.filter(y => y.id === x.dataObject.id).length === 0);
        toDispose.forEach(x => {
            this._removeFromShadowGenerator(x.drawableObject);
            x.dispose();
        });
        this[drawableListName] = this[drawableListName].filter(x => nearbyItems.filter(y => y.id === x.dataObject.id).length > 0);
    }

    _addToShadowGenerator(mesh) {
        this.shadowGenerator.addShadowCaster(mesh);
    }

    _removeFromShadowGenerator(mesh) {
        this.shadowGenerator.removeShadowCaster(mesh);
    }
}
